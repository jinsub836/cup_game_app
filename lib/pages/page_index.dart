import 'package:cup_game_app/components/cup_choice_item.dart';
import 'package:flutter/material.dart';

 class PageIndex extends StatefulWidget {
   const PageIndex({Key? key}) : super(key: key);

@override
State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  // GlobalKey 생성


  num _isStart = 0;
  num betMoney = 100;
  List<num> result = [0, 100, 200];
  String imgSrc = 'assets/yabayicup.png';

  void _gameStart() {
    setState(() {
      _isStart = 30;
      result.shuffle();
    });
  }

  void _gameEnd() {
    setState(() {
      _isStart = 200;
    });
  }
  void _resetCupChoiceItem() {
    setState(() {
      result.shuffle();
      cupChoiceItemKey.currentState?.reset();
      cupChoiceItemKey2.currentState?.reset();
      cupChoiceItemKey3.currentState?.reset();
    });
  }

  @override
  void initState(){
    super.initState();
    setState(() {
      result.shuffle();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('야바위:놀이'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    if (_isStart < betMoney) {
      return SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 200),
          child: Column(
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                        child: CupChoiceItem(key: cupChoiceItemKey,
                            betMoney: betMoney, currentMoney: result[0])),
                    Container(
                        child: CupChoiceItem( key: cupChoiceItemKey2,
                            betMoney: betMoney, currentMoney: result[1])),
                    Container(
                        child: CupChoiceItem( key: cupChoiceItemKey3,
                            betMoney: betMoney, currentMoney: result[2]))
                  ],
                ),
              ),
              Container(
                  margin: EdgeInsets.fromLTRB(10, 30, 10, 0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(30, 30, 10, 0),
                          child: OutlinedButton(
                            onPressed: () {
                              setState(() {
                                _resetCupChoiceItem();
                              });
                            },
                            child: const Text('다시 하기'),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(30, 30, 10, 0),
                          child: OutlinedButton(
                            onPressed: () {
                              _gameEnd();
                            },
                            child: const Text('종료'),
                          ),
                        )
                      ]))
            ],
          ),
        ),
      );
    }
    else {
      return SingleChildScrollView(
        child: Center(
          child: Container(
            margin: EdgeInsets.only(top: 300),
            child: OutlinedButton(
              onPressed: () {
                _gameStart();
              },
              child: const Text('시작'),
            ),
          ),
        ),
      );
    }
  }
}
