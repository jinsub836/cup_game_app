import 'package:flutter/material.dart';
import 'package:cup_game_app/pages/page_index.dart';

class CupChoiceItem extends StatefulWidget {
  const CupChoiceItem({
    Key? key, // key 매개변수를 추가합니다.
    required this.betMoney,
    required this.currentMoney,
  }) : super(key: key); // key를 상위 클래스에 전달합니다.

  final num betMoney; // 100
  final num currentMoney; // 0

  @override
  _CupChoiceItemState createState() => _CupChoiceItemState();
}

final GlobalKey<_CupChoiceItemState> cupChoiceItemKey =
GlobalKey<_CupChoiceItemState>();

final GlobalKey<_CupChoiceItemState> cupChoiceItemKey2 =
GlobalKey<_CupChoiceItemState>();

final GlobalKey<_CupChoiceItemState> cupChoiceItemKey3 =
GlobalKey<_CupChoiceItemState>();

class _CupChoiceItemState extends State<CupChoiceItem> {
  bool _isOpen = false;
  String imgSrc = 'assets/yabayicup.png';

  void _isChoice(){
    if(!_isOpen){
      setState(() {
        _isOpen = true;
      });}
  }


  void _calculateImgSrc(){
    //상자 열었을 때
    //판돈이 현재 돈보다 적을 때는 : bomb
    //판돈이 현재 돈과 같을때 : 100원
    //판돈이 현재 돈보다 많을때 : 200원

    //컵을 열지 않을 때 : 무조건 cup.img

    String tempImgSrc = '';
    if (_isOpen){
      if ( widget.betMoney  < widget.currentMoney){
        tempImgSrc ='assets/bomb-3175208_640.png';
      } else if(widget.betMoney  == widget.currentMoney){
        tempImgSrc = 'assets/AA.14521465.1.jpg';
      }
      else{ tempImgSrc = 'assets/200won.jpg';}
      }
    else { tempImgSrc = 'assets/yabayicup.png';}
    setState(() {
      imgSrc = tempImgSrc;
    });
  }

  void reset() {
    setState(() {
      _isOpen = false;
      imgSrc = 'assets/yabayicup.png';
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _isChoice();
        _calculateImgSrc();},
      child:
      SizedBox(
        width: 100,
        height: 100,
        child:
          Image.asset(imgSrc)
      )
      ,
    );
  }
}
